#ifndef PATIENTCARD_H
#define PATIENTCARD_H

#include "OphthalmologyDatabase/patientdbtable.h"
#include "newpatientdata.h"

#include <QDialog>
#include <QMessageBox>

namespace Ui {
class PatientCard;
}

class PatientCard : public QDialog
{
    Q_OBJECT

public:
    explicit PatientCard(QWidget *parent = 0);
    ~PatientCard();

    void setAmb(int _amb);
    void setUpdating(bool upd);

private slots:
    void on_pushButtonPatientCardDeny_clicked();

    void on_pushButtonPatientCardAccept_clicked();

    int on_pushButtonPatientCardSave_clicked();

    void loadFromExisting(int amb);

private:
    Ui::PatientCard *ui;

    PatientDbTable* dbTable;

    bool saved;
    bool updating;
    int amb;
};

#endif // PATIENTCARD_H
