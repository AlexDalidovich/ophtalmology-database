#include "epicrizdial.h"
#include "ui_epicrizdial.h"
#include "anamnezdial.h"

EpicrizDial::EpicrizDial(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EpicrizDial)
{
    ui->setupUi(this);
    this->setAttribute(Qt::WA_DeleteOnClose);
    dbTable = AbstractOperations::createDbConnection<EpicDbTable>();

    diagDialog = new AddingDialog(this);
    therDialog = new AddingDialog(this);


    connect(diagDialog,SIGNAL(accepted()),this,SLOT(addDiagnosis()));
    connect(therDialog,SIGNAL(accepted()),this,SLOT(addTherapy()));

    connect(ui->comboBox_2,SIGNAL(currentIndexChanged(int)),
            this,SLOT(selectItemFromDiag(int)));

    connect(ui->comboBox_4,SIGNAL(currentIndexChanged(int)),
            this,SLOT(selectItemFromTher(int)));

    updating = false;

    diagList = this->getListOfItems(ui->comboBox_2);
    therList = this->getListOfItems(ui->comboBox_4);

    this->updateBoxes();
}

EpicrizDial::~EpicrizDial()
{
    delete diagDialog;
    delete therDialog;
    delete ui;
}

void EpicrizDial::setPatientId(int &_id)
{
    id = _id;
    VisitDbTable* visitTable = AbstractOperations::createDbConnection<VisitDbTable>();
    QSqlQuery query = visitTable->getRows(tr("ID"),QString::number(id));
    query.next();
    visitTable->parseRow(query);
    QString doctorString = visitTable->getTextFields()["DOCTOR"];
    ui->lineEditEpicrizFioDoctor_2->setText(doctorString);
    int amb = visitTable->getIntFields()["AMB_NUM"];
    PatientDbTable* patTable = AbstractOperations::createDbConnection<PatientDbTable>();
    query = patTable->getRows(tr("AMB_NUM"),QString::number(amb));
    query.next();
    patTable->parseRow(query);
    QHash<QString,QString> textFields = patTable->getTextFields();
    ui->lineEditEpicrizPatientFio_2->setText( textFields[tr("LAST_NAME")] + " " +
            textFields[tr("FIRST_NAME")] + " " +
            textFields[tr("SECOND_NAME")]);

    query = dbTable->getRows(tr("ID"),QString::number(id));
    query.next();
    dbTable->parseRow(query);
    ui->plainTextEdit->setPlainText(dbTable->getTextFields()["DIAGNOSIS"]);
    ui->plainTextEdit_2->setPlainText(dbTable->getTextFields()["THERAPY"]);

}

void EpicrizDial::setUpdatingFlag(bool upd)
{
    updating = upd;

//    int loadId;
//    updating == true?
//                loadId = id : loadId = id - 1;
//    QSqlQuery query = dbTable->getRows(tr("ID"),QString::number(loadId));
//    query.next();
//    dbTable->parseRow(query);
//    ui->plainTextEdit->setPlainText(dbTable->getTextFields()["DIAGNOSIS"]);
//    ui->plainTextEdit_2->setPlainText(dbTable->getTextFields()["THERAPY_LIST"]);
}

void EpicrizDial::setPatientName(QString &name)
{
    ui->lineEditEpicrizPatientFio_2->setText(name);
}


void EpicrizDial::on_pushButtonEpicrizSave_2_clicked()
{
    //this->close();
    if(!updating&&ui->lineEditEpicrizFioDoctor_2->text().isEmpty())
    {
        QMessageBox::warning(this,"Внимание!","Вы не ввели ФИО доктора");
        return;
    }

    QStringList patientData;
    patientData << QString::number(id);
    patientData << ui->plainTextEdit->toPlainText();
    patientData << ui->plainTextEdit_2->toPlainText();

    if(!updating)
        dbTable->insertRow(patientData);
    else dbTable->changeTable(tr("ID"),patientData);

    QMessageBox::information(0,"Сохранение","Успех");

}

void EpicrizDial::on_pushButtonEpicrizAdmin_2_clicked()
{
    this->close();
}

void EpicrizDial::on_pushButtonEpicrizAnamnez_2_clicked()
{
    this->hide();
    emit(anamButtonSignal());
}

void EpicrizDial::on_pushButtonEpicrizAddDiagnos_2_clicked()
{
    diagDialog->clearInput();
    diagDialog->show();
}

void EpicrizDial::on_pushButtonEpicrizAddKursLecheniya_2_clicked()
{
    therDialog->clearInput();
    therDialog->show();
}

void EpicrizDial::addDiagnosis()
{
    diagDialog->close();
    qDebug()<<diagDialog->getText();
    DiagnosisDbtable* diagnosisTable = DiagnosisDbtable::getPtr();
    diagnosisTable->insertRow(diagDialog->getText());

    this->updateBoxes();
}

void EpicrizDial::addTherapy()
{
    therDialog->close();
    TherapyDbTable* therapyTable = TherapyDbTable::getPtr();
    therapyTable->insertRow(therDialog->getText());

    this->updateBoxes();
}

void EpicrizDial::updateBoxes()
{
    ui->comboBox_2->clear();
    ui->comboBox_4->clear();

    ui->comboBox_2->addItems(diagList);
    ui->comboBox_4->addItems(therList);

    TherapyDbTable* therapyTable = TherapyDbTable::getPtr();
    QStringList list = therapyTable->getRows();
    foreach (QString row, list) {
        ui->comboBox_4->addItem(row);
    }

    DiagnosisDbtable* diagnosisTable = DiagnosisDbtable::getPtr();
    list = diagnosisTable->getRows();
    foreach (QString row, list) {
        ui->comboBox_2->addItem(row);
    }
}

void EpicrizDial::selectItemFromDiag(int index)
{
    if(!ui->comboBox_2->currentText().isEmpty())
    {
        QString diagnosis = ui->plainTextEdit->toPlainText();
        diagnosis += ui->comboBox_2->currentText() + "; ";
        ui->plainTextEdit->setPlainText(diagnosis);
    }
}

void EpicrizDial::selectItemFromTher(int index)
{
    if(!ui->comboBox_4->currentText().isEmpty())
    {
        QString therapy = ui->plainTextEdit_2->toPlainText();
        therapy += ui->comboBox_4->currentText() + "; ";
        ui->plainTextEdit_2->setPlainText(therapy);
    }
}

QStringList EpicrizDial::getListOfItems(QComboBox *box)
{
    QStringList list;
    for(int i = 0 ; i < box->count() ; i++)
        list << box->itemText(i);
    return list;
}
