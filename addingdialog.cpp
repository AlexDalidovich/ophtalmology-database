#include "addingdialog.h"
#include "ui_addingdialog.h"

AddingDialog::AddingDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddingDialog)
{
    ui->setupUi(this);
    ui->lineEdit->setParent(this);
//    ui->lineEdit->setReadOnly(false);
}

AddingDialog::~AddingDialog()
{
    delete ui;
}

QString AddingDialog::getText()
{
    return ui->lineEdit->text();
}

void AddingDialog::clearInput()
{
    ui->lineEdit->clear();
}
