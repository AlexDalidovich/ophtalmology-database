#include "patientcard.h"
#include "ui_patientcard.h"

PatientCard::PatientCard(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PatientCard)
{
    ui->setupUi(this);
    this->setAttribute(Qt::WA_DeleteOnClose);
    saved = false;
    updating = false;
    dbTable = AbstractOperations::createDbConnection<PatientDbTable>();

    QRegExp forNumbers("^[0-9]{0,20}$");
    ui->lineEditPatientCardPassportID->setValidator(new QRegExpValidator(forNumbers));
    ui->lineEditPatientCardNumber->setValidator(new QRegExpValidator(forNumbers));
}

PatientCard::~PatientCard()
{
    delete ui;
}

void PatientCard::setAmb(int _amb)
{
    amb = _amb;
    this->loadFromExisting(amb);
}

void PatientCard::setUpdating(bool upd)
{
    updating = upd;
    ui->lineEditPatientCardNumber->setReadOnly(true);
    ui->pushButtonPatientCardAccept->setHidden(true);
}

void PatientCard::on_pushButtonPatientCardDeny_clicked()
{
    this->close();
}

void PatientCard::on_pushButtonPatientCardAccept_clicked()
{
    if(!saved)
    {
        QMessageBox::warning(NULL,"ВНИМАНИЕ!","Пожалуйста,сохраните/создайте форму данных о пациенте");
        return;
    }
    newPatientData *test = new newPatientData(this);
    test->setAmbNum(amb);
    this->hide();
    test->show();
}

int PatientCard::on_pushButtonPatientCardSave_clicked()
{
    if(saved&&!updating)
        if(QMessageBox::Yes == QMessageBox::question(this,"Операция окончена?",
                              "Запись была сохранена. \n"
                              "Сохранить повторно?"))
            return -1;
     if(ui->lineEditPatientCardNumber->text().isEmpty())
     {
         QMessageBox::information(this,"Проверьте данные","Заполните поле \"Номер карточки\" ");
         return -1;
     }

     int sex;
     if(ui->radioButtonPatientCardMan->isChecked())
         sex = 1;
     else
         if(ui->radioButtonPatientCardWoman->isChecked())
             sex = 2;
         else   {
             QMessageBox::information(this,"Проверьте данные","Заполните поле \"Пол\" ");
             return -1;
         }



     QStringList patientData;

     patientData << ui->lineEditPatientCardNumber->text();
     patientData << ui->dateEditPatientCardBtirhday->text();
     patientData << QString::number(sex);
     patientData << ui->lineEditPatientCardAddres->text();
     patientData << ui->lineEditPatientCardPhoneNum->text();
     patientData << ui->lineEditPatientCardMestoRab->text();
     patientData << ui->lineEditPatientCardDolzhnost->text();
     patientData << ui->lineEditPatientCardPassportSeriya->text();
     patientData << ui->lineEditPatientCardPassportID->text();
     patientData << ui->dateEditPatientCardVidan->text();
     patientData << ui->lineEditPatientCardVidan->text();
     patientData << ui->plainTextEditPatientCardInformation->toPlainText();
     patientData << ui->lineEditPatientCardName->text();
     patientData << ui->lineEditPatientCardOtchestvo->text();
     patientData << ui->lineEditPatientCardFamiliya->text();


    if(QMessageBox::No == QMessageBox::question(this,"Операция окончена?",
                                                "Вы заполнили все поля данных?"))
        return -1;
    amb = ui->lineEditPatientCardNumber->text().toInt();
    if(updating)
        dbTable->changeTable(tr("AMB_NUM"),patientData);
    else
        if(dbTable->insertRow(patientData) != -1)
        {
            QMessageBox::warning(this,"Error","Карта с таким номером уже существует");
        }
        else
            saved = true;
    return 0;
}

void PatientCard::loadFromExisting(int amb)
{
    QSqlQuery query = dbTable->getRows(tr("AMB_NUM"),QString::number(amb));
    query.next();
    dbTable->parseRow(query);
    QHash<QString,QString> textFields = dbTable->getTextFields();
    QHash<QString,int> intFields = dbTable->getIntFields();
    ui->lineEditPatientCardNumber->setText(QString::number(amb));
    ui->lineEditPatientCardFamiliya->setText(textFields["LAST_NAME"]);
    ui->lineEditPatientCardOtchestvo->setText(textFields["SECOND_NAME"]);
    ui->lineEditPatientCardName->setText(textFields["FIRST_NAME"]);
    ui->dateEditPatientCardBtirhday->setDate(QDate::fromString(textFields["BIRTHDAY"],"dd.MM.yyyy"));
    ui->radioButtonPatientCardMan->setCheckable(true);
    ui->radioButtonPatientCardWoman->setCheckable(true);
    if(intFields["SEX"] == 1)
        ui->radioButtonPatientCardMan->setChecked(true);
    else
        ui->radioButtonPatientCardWoman->setChecked(true);
    ui->lineEditPatientCardAddres->setText(textFields["ADDRESS"]);
    ui->lineEditPatientCardPhoneNum->setText(textFields["TEL"]);
    ui->lineEditPatientCardMestoRab->setText(textFields["JOB"]);
    ui->lineEditPatientCardDolzhnost->setText(textFields["POST"]);
    ui->lineEditPatientCardPassportSeriya->setText(textFields["PAS_SERIE"]);
    ui->lineEditPatientCardPassportID->setText(QString::number(intFields["PAS_NUM"]));

    QString dateString = textFields["PAS_GIVEN"];
    QDate date = QDate::fromString(dateString,"dd.MM.yyyy");
    ui->dateEditPatientCardVidan->setDate(date);

    ui->lineEditPatientCardVidan->setText(textFields["PAS_ADDRESS_GIVEN"]);
    ui->plainTextEditPatientCardInformation->setPlainText(textFields["ADD_INFO"]);

}
