#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableWidget_2->hideColumn(2);

    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("Patient.sqlite");
    db.setUserName("user");
    db.setPassword("0");
    if(!db.open())
    {
        QMessageBox::critical(NULL,"ФАТАЛЬНАЯ ОШИБКА ПРИЛОЖЕНИЯ","Невозможно открыть базу данных./nПриложение будет завершено"
                        "/nОбратитесь к системному администратору");
        QApplication::quit();
    };


    patientDb = AbstractOperations::createDbConnection<PatientDbTable>();
    anamnesisDb = AbstractOperations::createDbConnection<AnamnesisDbTable>();
    epicrisisDb = AbstractOperations::createDbConnection<EpicDbTable>();
    visitDb = AbstractOperations::createDbConnection<VisitDbTable>();
    imagesDb = AbstractOperations::createDbConnection<ImagesDbTable>();

    patientDb->createTable();
    visitDb->createTable();
    imagesDb->createTable();
    anamnesisDb->createTable();
    epicrisisDb->createTable();

    connect(patientDb,SIGNAL(update()),this,SLOT(loadDataFromPatientDb()));
    connect(visitDb,SIGNAL(update()),this,SLOT(loadDataFromVisitDb()));
    connect(ui->lineEdit,SIGNAL(textChanged(QString)),this,SLOT(findByTextInName(QString)));
    connect(ui->lineEdit_2,SIGNAL(textChanged(QString)),this,SLOT(findByTextInDiag(QString)));

    loadDataOnStart();
    ui->tab->setDisabled(true);
    ui->tab_2->setDisabled(true);
    ui->tab_3->setDisabled(true);
}

MainWindow::~MainWindow()
{
    delete patientDb;
    delete anamnesisDb;
    delete epicrisisDb;
    delete visitDb;
    delete imagesDb;

    db.close();
    delete ui;
}

void MainWindow::loadDataOnStart()
{
    this->loadDataFromPatientDb();
}

void MainWindow::loadDataFromPatientDb()
{
    this->clearTable(ui->tableWidget);

    QSqlQuery query = patientDb->getFullTable();

    while(query.next())
    {
        int count = ui->tableWidget->rowCount();
        ui->tableWidget->insertRow(count);
        patientDb->parseRow(query);
        QTableWidgetItem* item;
        QHash<QString,QString> textFields = patientDb->getTextFields();
        QHash<QString,int> intFields = patientDb->getIntFields();

        item = new QTableWidgetItem(QString::number(intFields[tr("AMB_NUM")]));
        item->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
        ui->tableWidget->setItem(count,0,item);

        item = new QTableWidgetItem
                (
                    textFields[tr("LAST_NAME")] + " " +
                    textFields[tr("FIRST_NAME")] + " " +
                    textFields[tr("SECOND_NAME")]
                );
        item->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
        ui->tableWidget->setItem(count,1,item);
    }
}

void MainWindow::loadDataFromVisitDb()
{
    int row  = ui->tableWidget->currentRow();
    if(row > -1)
    {
        this->clearTable(ui->tableWidget_2);
        this->clearTable(ui->tableWidget_3);
        idMapper.clear();

        QTableWidgetItem* item;

        QString amb = getSelectedCell(ui->tableWidget,row,0);
        QSqlQuery query = visitDb->getRows(tr("AMB_NUM"),amb);
        while(query.next())
        {
            int count = ui->tableWidget_2->rowCount();
            ui->tableWidget_2->insertRow(count);
            visitDb->parseRow(query);

            QHash<QString,QString> textFields = visitDb->getTextFields();
            QHash<QString,int> intFields = visitDb->getIntFields();

            item = new QTableWidgetItem(textFields[tr("DATE")]);
            idMapper[item] = intFields[tr("ID")];
            item->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
            ui->tableWidget_2->setItem(count,0,item);

        }
    }
}

void MainWindow::loadDataFromEpicrisisDb()
{
    int id;
    foreach (QTableWidgetItem* item, idMapper.keys()) {
        if(ui->tableWidget_2->selectedItems().contains(item))
            id = idMapper[item];
    }
    QSqlQuery query = epicrisisDb->getRows(tr("ID"),QString::number(id));
    query.next();
    epicrisisDb->parseRow(query);
    QHash<QString,QString> textFields = epicrisisDb->getTextFields();
    ui->plainTextEditEpicrizDiagnos->setPlainText(textFields[tr("DIAGNOSIS")]);
    ui->plainTextEditEpicrizKursLecheniya->setPlainText(textFields[tr("THERAPY")]);

    query = visitDb->getRows(tr("ID"),QString::number(id));
    query.next();
    visitDb->parseRow(query);
    ui->lineEditEpicrizFioDoctor->setText(visitDb->getTextFields()[tr("DOCTOR")]);
}

void MainWindow::loadDataFromAnamnesisDb()
{
    int id;
    foreach (QTableWidgetItem* item, idMapper.keys()) {
        if(ui->tableWidget_2->selectedItems().contains(item))
            id = idMapper[item];
    }qDebug()<<"main id"<<id;
    QSqlQuery query = anamnesisDb->getRows(tr("ID"),QString::number(id));
    query.next();
    anamnesisDb->parseRow(query);
    QHash<QString,QString> textFields = anamnesisDb->getTextFields();
    QHash<QString,float> floatFields =  anamnesisDb->getFloatFields();
    ui->lineEditAnamnezLeftEye->setText(QString::number(floatFields["EYE_L"]));
    ui->lineEditAnamnezRightEye->setText(QString::number(floatFields["EYE_R"]));
    ui->lineEditAnamnezInEyePressure->setText(QString::number(floatFields["PRESSURE"]));
    ui->plainTextEditAnamnezZhaloba->setPlainText(textFields["COMPLAINS"]);
    ui->plainTextEditAnamnezRazvitieZabolevaniya->setPlainText(textFields["DISEASES_PROGRESS"]);
    ui->plainTextEditAnamnezSoputstvushieZab->setPlainText(textFields["CURRENT_DISEASES"]);
    ui->plainTextEditAnamnezPerenesenZab->setPlainText(textFields["LAST_DISEASES"]);
    ui->plainTextEditAnamnezNasled->setPlainText(textFields["GENETICS"]);
    ui->plainTextEditAnamnezNeperenos->setPlainText(textFields["INTOLERANCE"]);
    ui->plainTextEditAnamnezVrednPriv->setPlainText(textFields["BAD_HABBITS"]);
    ui->plainTextEditAnamnezBitovieUsl->setPlainText(textFields["LIFE_CONDITIONS"]);

    query = visitDb->getRows(tr("ID"),QString::number(id));
    query.next();
    visitDb->parseRow(query);
    QString dateString = visitDb->getTextFields()["DATE"];
    int visitNum = visitDb->getIntFields()["VISIT_NUM"];
    ui->label_36->setText(QString::number(visitNum));
    ui->dateEditAnamnez->setDate(QDate::fromString(dateString.remove(QChar(' '),Qt::CaseInsensitive),"dd.MM.yyyy"));
}

void MainWindow::loadDataFromImageDb()
{

}

QString &MainWindow::getSelectedCell(QTableWidget *widget , int row , int column)
{
    return widget->item(row,column)->text();
}

void MainWindow::clearTable(QTableWidget *table)
{
    while(table->rowCount() > 0)
        table->removeRow(0);
}

void MainWindow::on_pushButtonNewPatient_clicked()
{
    if(!ui->tableWidget->selectedItems().isEmpty())
    {
            newPatientData *test = new newPatientData;
            int amb = ui->tableWidget->selectedItems()[0]->text().toInt();
            test->setAmbNum(amb);
            test->show();
    }
}

void MainWindow::on_pushButtonOpenPatientData_clicked()
{
    PatientCard *test = new PatientCard;
    test->show();
}

void MainWindow::on_pushButtonPatientCardOpen_clicked()
{

}

void MainWindow::on_pushButtonEditPatient_clicked()
{

        int id;
        foreach (QTableWidgetItem* item, idMapper.keys()) {
            if(ui->tableWidget_2->selectedItems().contains(item))
                id = idMapper[item];
        }
        newPatientData *test = new newPatientData;
        QString amb = ui->tableWidget->item(ui->tableWidget->currentRow(),0)->text();
        test->setAmbNum(amb.toInt());
        test->setPatientID(id);
        test->show();

}

void MainWindow::on_tableWidget_cellClicked(int row, int column)
{

    QString amb = ui->tableWidget->item(row,0)->text();
    QSqlQuery query = patientDb->getRows(tr("AMB_NUM"),amb);
    query.next();
    patientDb->parseRow(query);
    QHash<QString,QString> textFields = patientDb->getTextFields();
    QHash<QString,int> intFields = patientDb->getIntFields();
    ui->lineEditPatientCardNumber->setText(amb);
    ui->lineEditPatientCardFamiliya->setText(textFields["LAST_NAME"]);
    ui->lineEditPatientCardOtchestvo->setText(textFields["SECOND_NAME"]);
    ui->lineEditPatientCardName->setText(textFields["FIRST_NAME"]);
    ui->dateEditPatientCardBtirhday->setDate(QDate::fromString(textFields["BIRTHDAY"].remove(" "),"dd.MM.yyyy"));
    ui->radioButtonPatientCardMan->setCheckable(true);
    ui->radioButtonPatientCardWoman->setCheckable(true);
    if(intFields["SEX"] == 1)
    {
        ui->radioButtonPatientCardMan->setChecked(true);
        ui->radioButtonPatientCardWoman->setCheckable(false);
    }
    else
    {
        ui->radioButtonPatientCardWoman->setChecked(true);
        ui->radioButtonPatientCardMan->setCheckable(false);
    }
    ui->lineEditPatientCardAddres->setText(textFields["ADDRESS"]);
    ui->lineEditPatientCardPhoneNum->setText(textFields["TEL"]);
    ui->lineEditPatientCardMestoRab->setText(textFields["JOB"]);
    ui->lineEditPatientCardDolzhnost->setText(textFields["POST"]);
    ui->lineEditPatientCardPassportSeriya->setText(textFields["PAS_SERIE"]);
    ui->lineEditPatientCardPassportID->setText(QString::number(intFields["PAS_NUM"]));
    ui->dateEditPatientCardVidan->setDate(QDate::fromString(textFields["PAS_GIVEN"].remove(" "),"dd.MM.yyyy"));
    ui->lineEditPatientCardVidan->setText(textFields["PAS_ADDRESS_GIVEN"]);
    ui->plainTextEditPatientCardInformation->setPlainText(textFields["ADD_INFO"]);
    ui->pushButtonEditPatient->setDisabled(true);
    ui->pushButton->setDisabled(true);

    ui->tab_3->setEnabled(true);
    ui->tab->setDisabled(true);
    ui->tab_2->setDisabled(true);

    ui->label_22->clear();
    this->loadDataFromVisitDb();
}

void MainWindow::on_tableWidget_2_cellClicked(int row, int column)
{
    clearTable(ui->tableWidget_3);

    foreach (QTableWidgetItem* item, idMapper.keys()) {
        if(ui->tableWidget_2->selectedItems().contains(item))
        {
            int id;
            id = idMapper[item];

            QSqlQuery query = imagesDb->getRows(tr("ID"),QString::number(id));

            while(query.next())
            {
                imagesDb->parseRow(query);
                QTableWidgetItem* imItem = new QTableWidgetItem(imagesDb->getTextFields()["RESEARCH"]);
                int id_research = imagesDb->getIntFields()["ID_RESEARCH"];
                ui->tableWidget_3->insertRow(ui->tableWidget_3->rowCount());
                ui->tableWidget_3->setItem(ui->tableWidget_3->rowCount()-1,0,imItem);
                imMapper[imItem] = id_research;
            }

        }
    }

    if(ui->tableWidget_2->selectedItems().length())
    {
        ui->pushButtonEditPatient->setEnabled(true);
        ui->pushButton->setEnabled(true);
    }
    QString& name = ui->tableWidget->selectedItems()[1]->text();
    ui->lineEditEpicrizPatientFio->setText(name);


    ui->tab->setEnabled(true);
    ui->tab_2->setEnabled(true);

    loadDataFromAnamnesisDb();
    loadDataFromEpicrisisDb();
}

void MainWindow::on_tableWidget_3_cellClicked(int row, int column)
{
    if(!ui->tableWidget_3->selectedItems().isEmpty())
    {
        int id;
        foreach (QTableWidgetItem* item, imMapper.keys()) {
            if(ui->tableWidget_3->selectedItems().contains(item))
                id = imMapper[item];
        }

        QSqlQuery query = imagesDb->getRows(tr("ID_RESEARCH"),QString::number(id));
        query.next();
        imagesDb->parseRow(query);

        QHash<QString,int> intFields = imagesDb->getIntFields();
        int w = intFields[tr("WIDTH")];
        int h = intFields[tr("HEIGHT")];

        QByteArray tempArray = imagesDb->getImageFields().begin().value();

        QImage image((uchar*)tempArray.data(),w,h,QImage::Format_RGB888);
        image = image.rgbSwapped().scaled(ui->scrollArea->size());
        ui->label_22->resize(image.size());
        ui->label_22->setPixmap(QPixmap::fromImage(image));
    }
}

void MainWindow::on_pushButton_5_clicked()
{
    if(ui->tableWidget_3->selectedItems().length())
    {
        int id;
        foreach (QTableWidgetItem* item, imMapper.keys()) {
            if(ui->tableWidget_3->selectedItems().contains(item))
                id = imMapper[item];
        }

        QSqlQuery query = imagesDb->getRows(tr("ID_RESEARCH"),QString::number(id));
        query.next();
        imagesDb->parseRow(query);

        QHash<QString,int> intFields = imagesDb->getIntFields();
        int w = intFields[tr("WIDTH")];
        int h = intFields[tr("HEIGHT")];

        QByteArray tempArray = imagesDb->getImageFields().begin().value();

        QImage image((uchar*)tempArray.data(),w,h,QImage::Format_RGB888);
        image = image.rgbSwapped();

        imLabel.setParent(this,Qt::Window);
        imLabel.setScaledContents(true);
        imLabel.setPixmap(QPixmap::fromImage(image));
        imLabel.show();
    }
}

void MainWindow::on_pushButton_4_clicked()
{
    if(ui->tableWidget_3->selectedItems().length())
    {
        int id;
        foreach (QTableWidgetItem* item, imMapper.keys()) {
            if(ui->tableWidget_3->selectedItems().contains(item))
                id = imMapper[item];
        }
        imagesDb->deleteRow(tr("ID_RESEARCH"),QString::number(id));

        this->on_tableWidget_2_cellClicked(0,0);
        ui->label_22->clear();
    }
}

void MainWindow::findByTextInName(QString text)
{
    this->clearTable(ui->tableWidget_2);
    this->clearTable(ui->tableWidget_3);

    ui->tab->setDisabled(true);
    ui->tab_2->setDisabled(true);
    ui->tab_3->setDisabled(true);

    ui->label_22->clear();

    if(text.isEmpty())
        for(int i = 0; i < ui->tableWidget->rowCount(); i++)
            ui->tableWidget->showRow(i);
    else
    {
        ui->lineEdit_2->clear();
        for(int i = 0; i < ui->tableWidget->rowCount(); i++)
        {
            if(!ui->tableWidget->item(i , 1)->text().contains(text,Qt::CaseInsensitive))
                ui->tableWidget->hideRow(i);
            else
                ui->tableWidget->showRow(i);
        }
    }
}

void MainWindow::findByTextInDiag(QString text)
{
    this->clearTable(ui->tableWidget_2);
    this->clearTable(ui->tableWidget_3);

    ui->tab->setDisabled(true);
    ui->tab_2->setDisabled(true);
    ui->tab_3->setDisabled(true);

    ui->label_22->clear();

    if(text.isEmpty())
        for(int i = 0; i < ui->tableWidget->rowCount(); i++)
            ui->tableWidget->showRow(i);
    else
    {
        ui->lineEdit->clear();
        for(int i = 0; i < ui->tableWidget->rowCount(); i++)
            ui->tableWidget->hideRow(i);

        QString q = "SELECT ID FROM VISITS CUR WHERE NOT EXISTS (SELECT * FROM VISITS ITER WHERE CUR.AMB_NUM = ITER.AMB_NUM AND ITER.ID > CUR.ID ) " ;

        QSqlQuery qry = epicrisisDb->query(q);
        while(qry.next())
        {
        epicrisisDb->parseRow(qry);
        qDebug()<<"ID"<<epicrisisDb->getIntFields()["ID"];
        }

        QString queryText = "SELECT *"
                " FROM EPICRISIS"
                " INNER JOIN (SELECT ID FROM VISITS CUR WHERE NOT EXISTS (SELECT * FROM VISITS ITER WHERE CUR.AMB_NUM = ITER.AMB_NUM AND ITER.ID > CUR.ID )) V ON "
                "EPICRISIS.ID = V.ID";
        qDebug()<<queryText;
        QSqlQuery query = epicrisisDb->query(queryText);
        while(query.next())
        {
            qDebug()<<query.value(query.record().indexOf(0)).toString();
            epicrisisDb->parseRow(query);
            if(epicrisisDb->getTextFields()["DIAGNOSIS"].replace(';',' ').contains(text,Qt::CaseInsensitive))
            {
                int id =  epicrisisDb->getIntFields()["ID"];
                QSqlQuery visQuery = visitDb->getRows(tr("ID"),QString::number(id));
                visQuery.next();
                visitDb->parseRow(visQuery);
                QString amb = visQuery.value(visQuery.record().indexOf("AMB_NUM")).toString();

                QTableWidgetItem* item = ui->tableWidget->findItems(amb,Qt::MatchContains)[0];
                ui->tableWidget->showRow(item->row());
            }
        }
    }
}

void MainWindow::on_pushButton_clicked()
{
    QPrinter* printer;
    printer = new QPrinter(QPrinter::PrinterResolution);
    printer->setOutputFormat(QPrinter::NativeFormat);
    printer->setPaperSize(QPrinter::A4);
    printer->setFullPage(true);
    printer->setResolution(300);


    int row = ui->tableWidget->currentRow();
    QString amb = ui->tableWidget->item(row,0)->text();

    QSqlQuery query = patientDb->getRows(tr("AMB_NUM"),amb);
    query.next();
    patientDb->parseRow(query);

    QHash<QString,QString> textFields = patientDb->getTextFields();
    QHash<QString,int> intFields = patientDb->getIntFields();

    QString printText = "Личные данные пациента: \n";
    printText += "Фамилия: " + textFields["LAST_NAME"] + "\n";
    printText += "Имя: " + textFields["FIRST_NAME"] + "\n";
    printText += "Отчество: " + textFields["SECOND_NAME"] + "\n";
    printText += "Номер амбулаторной карты: " + amb + "\n";
    printText += "Дата рождения: " + textFields["BIRTHDAY"] + "\n";
    intFields["SEX"] == 1?
        printText += "Пол: мужской \n":  printText += "Пол: женский \n";

    printText += "Адрес: " + textFields["ADDRESS"] + "\n";
    printText += "Телефон: " + textFields["TEL"] + "\n";
    printText += "Место работы: " + textFields["JOB"] + "\n";
    printText += "Должность: " + textFields["POST"] + "\n";
    printText += "Cерия паспорта: " + textFields["PAS_SERIE"] + "\n";
    printText += "Номер паспорта: " + QString::number(intFields["PAS_NUM"]) + "\n";
    printText += "Дата выдачи паспорта: " + textFields["PAS_GIVEN"] + "\n";
    printText += "Паспорт выдан: " + textFields["PAS_ADDRESS_GIVEN"] + "\n";
    printText += "Дополнительная информация: " + textFields["ADD_INFO"] + "\n\n\n\n";

    int id;
    foreach (QTableWidgetItem* item, idMapper.keys()) {
        if(ui->tableWidget_2->selectedItems().contains(item))
            id = idMapper[item];
    }

    query = visitDb->getRows(tr("ID"),QString::number(id));
    query.next();
    visitDb->parseRow(query);

    printText += "Данные о визите: \n";
    printText += "Номер визита: " + QString::number(visitDb->getIntFields()["VISIT_NUM"]) + "\n";
    printText += "Дата: " + visitDb->getTextFields()["DATE"] + "\n";
    printText += "Врач: " + visitDb->getTextFields()["DOCTOR"] + "\n\n";

    query = anamnesisDb->getRows(tr("ID"),QString::number(id));
    query.next();
    anamnesisDb->parseRow(query);

    QHash<QString,float> floatFields = anamnesisDb->getFloatFields();
    intFields = anamnesisDb->getIntFields();
    textFields = anamnesisDb->getTextFields();

    printText += "Анамнез: \n";
    printText += "Жалобы: " + textFields["COMPLAINS"] + "\n";
    printText += "Острота зрения: левый [" + QString::number(floatFields["EYE_L"]) +
            "] , правый [" + QString::number(floatFields["EYE_R"]) + "]\n";
    printText += "Внутриглазное давление: " + QString::number(floatFields["PRESSURE"]) + "\n\n";
    printText += "Непереносимость: " + textFields["INTOLERANCE"] + "\n";
    printText += "Перенесенные заболевания: " + textFields["LAST_DISEASES"] + "\n";
    printText += "Сопутствующие болезни: " + textFields["CURRENT_DISEASES"] + "\n";
    printText += "Наследственность: " + textFields["GENETICS"] + "\n";
    printText += "Развитие болезни: " + textFields["DISEASES_PROGRESS"] + "\n";
    printText += "Вредные привычки: " + textFields["BAD_HABBITS"] + "\n";
    printText += "Бытовые условия: " + textFields["LIFE_CONDITIONS"] + "\n";

    query = epicrisisDb->getRows(tr("ID"),QString::number(id));
    query.next();
    epicrisisDb->parseRow(query);

    printText += "Эпикриз: \n";

    printText += "Диагноз: " + epicrisisDb->getTextFields()["DIAGNOSIS"] + "\n";
    printText += "Курс лечения: " + epicrisisDb->getTextFields()["THERAPY"] + "\n";

    QTextDocument doc(printText);

    QPrintDialog dlg(printer,0);
    if(dlg.exec() == QDialog::Accepted)
    {
       doc.print(printer);
    }
    delete printer;
}

void MainWindow::resizeEvent(QResizeEvent *event)
{
    this->on_tableWidget_3_cellClicked(0,0);
}
