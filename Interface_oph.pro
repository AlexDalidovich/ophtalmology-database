#-------------------------------------------------
#
# Project created by QtCreator 2014-07-21T18:45:52
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = Interface_oph
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    newpatientdata.cpp \
    epicrizdial.cpp \
    anamnezdial.cpp \
    patientcard.cpp \
    OphthalmologyDatabase/patientdbtable.cpp \
    OphthalmologyDatabase/anamnesisdbtable.cpp \
    OphthalmologyDatabase/epicdbtable.cpp \
    OphthalmologyDatabase/visitdbtable.cpp \
    OphthalmologyDatabase/imagesdbtable.cpp \
    OphthalmologyDatabase/researchdbtable.cpp \
    OphthalmologyDatabase/abstractoperations.cpp \
    OphthalmologyDatabase/diagnosisdbtable.cpp \
    addingdialog.cpp \
    fileopendialog.cpp \
    OphthalmologyDatabase/therapydbtable.cpp

HEADERS  += mainwindow.h \
    newpatientdata.h \
    epicrizdial.h \
    anamnezdial.h \
    patientcard.h \
    OphthalmologyDatabase/patientdbtable.h \
    OphthalmologyDatabase/anamnesisdbtable.h \
    OphthalmologyDatabase/epicdbtable.h \
    OphthalmologyDatabase/visitdbtable.h \
    OphthalmologyDatabase/imagesdbtable.h \
    OphthalmologyDatabase/researchdbtable.h \
    OphthalmologyDatabase/abstractoperations.h \
    OphthalmologyDatabase/diagnosisdbtable.h \
    addingdialog.h \
    fileopendialog.h \
    OphthalmologyDatabase/therapydbtable.h

FORMS    += mainwindow.ui \
    newpatientdata.ui \
    epicrizdial.ui \
    anamnezdial.ui \
    patientcard.ui \
    addingdialog.ui

LIBS += -LD:/job/OpenCV_include-2.4.8/lib \
        -lopencv_highgui248d \
        -lopencv_core248d \
        -lopencv_imgproc248d


INCLUDEPATH += $$PWD/../../OpenCV_include-2.4.8/include
