#include "fileopendialog.h"

FileOpenDialog::FileOpenDialog(QWidget *parent,
                               const QString &caption,
                               const QString &directory,
                               const QString &filter)
    :QWidget(parent)
{
    layout = new QVBoxLayout;
    dialog = new QFileDialog(0,caption,directory,filter);
    comboBox = new QComboBox;
    connect(dialog,SIGNAL(rejected()),this,SLOT(close()));
    connect(dialog,SIGNAL(accepted()),this,SIGNAL(accepted()));
    connect(dialog,SIGNAL(rejected()),this,SIGNAL(rejected()));

    comboBox->addItem(tr("OKT"));
    comboBox->addItem(tr("FAG"));
    comboBox->addItem(tr("AUTO"));

    layout->addWidget(dialog);
    layout->addWidget(comboBox);
    this->setLayout(layout);
    this->setMinimumSize(600,400);
}

FileOpenDialog::~FileOpenDialog()
{

}

QString FileOpenDialog::getFilePath()
{
    return dialog->selectedFiles().first();
}

QString FileOpenDialog::getImageType()
{
    return comboBox->currentText();
}

void FileOpenDialog::closeEvent(QCloseEvent *)
{
    parentWidget()->setEnabled(true);
}
