#include "ui_anamnezdial.h"
#include "anamnezdial.h"

 AnamnezDial::AnamnezDial(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AnamnezDial)
{
     QRegExp forText("^[-0-9a-zA-ZА-Яа-я.]$");
     QRegExp forNumbers("^[0-9]{0,20}$");
     QRegExp forReal("^[-]?[0-9]*[\.]?[0-9]+$");


     ui->setupUi(this);
     updating = false;

     ui->lineEditAnamnezInEyePressure->setValidator(new QRegExpValidator(forReal, this));
     ui->lineEditAnamnezLeftEye->setValidator(new QRegExpValidator(forReal, this));
     ui->lineEditAnamnezRightEye->setValidator(new QRegExpValidator(forReal, this));

     dbTable = AbstractOperations::createDbConnection<AnamnesisDbTable>();
     this->setAttribute(Qt::WA_DeleteOnClose);
 }

AnamnezDial::~AnamnezDial()
{
    delete ui;
}

void AnamnezDial::setPatientId(int &_id)
{
    id = _id;
    VisitDbTable* tempTable =  AbstractOperations::createDbConnection<VisitDbTable>();
    QSqlQuery query = tempTable->getRows(tr("ID"),QString::number(id));
    query.next();
    tempTable->parseRow(query);
    ui->label_37->setText(QString::number(tempTable->getIntFields()["VISIT_NUM"]));
    qDebug()<<tempTable->getTextFields()["DATE"];
    QDate date = QDate::fromString(tempTable->getTextFields()["DATE"].remove(QChar(' '),Qt::CaseInsensitive),"dd.MM.yyyy");
    qDebug()<<date;
    ui->dateEditAnamnez->setDate(date);

    query = dbTable->getRows(tr("ID"),QString::number(id));
    query.next();
    dbTable->parseRow(query);
    QHash<QString,QString> textFields = dbTable->getTextFields();
    QHash<QString,float> floatFields =  dbTable->getFloatFields();
    ui->lineEditAnamnezLeftEye->setText(QString::number(floatFields["EYE_L"]));
    ui->lineEditAnamnezRightEye->setText(QString::number(floatFields["EYE_R"]));
    ui->lineEditAnamnezInEyePressure->setText(QString::number(floatFields["PRESSURE"]));
    ui->plainTextEditAnamnezZhaloba->setPlainText(textFields["COMPLAINS"]);
    ui->plainTextEditAnamnezRazvitieZabolevaniya->setPlainText(textFields["DISEASES_PROGRESS"]);
    ui->plainTextEditAnamnezSoputstvushieZab->setPlainText(textFields["CURRENT_DISEASES"]);
    ui->plainTextEditAnamnezPerenesenZab->setPlainText(textFields["LAST_DISEASES"]);
    ui->plainTextEditAnamnezNasled->setPlainText(textFields["GENETICS"]);
    ui->plainTextEditAnamnezNeperenos->setPlainText(textFields["INTOLERANCE"]);
    ui->plainTextEditAnamnezVrednPriv->setPlainText(textFields["BAD_HABBITS"]);
    ui->plainTextEditAnamnezBitovieUsl->setPlainText(textFields["LIFE_CONDITIONS"]);
}

void AnamnezDial::setUpdatingFlag(bool upd)
{
    updating = upd;
}

void AnamnezDial::on_pushButtonAnamnezSaveImage_clicked()
{
    QStringList patientData;
    patientData << QString::number(id);
    patientData << ui->plainTextEditAnamnezZhaloba->toPlainText();
    patientData << ui->lineEditAnamnezInEyePressure->text();
    patientData << ui->lineEditAnamnezLeftEye->text();
    patientData << ui->lineEditAnamnezRightEye->text();
    patientData << ui->plainTextEditAnamnezNeperenos->toPlainText();
    patientData << ui->plainTextEditAnamnezPerenesenZab->toPlainText();
    patientData << ui->plainTextEditAnamnezSoputstvushieZab->toPlainText();
    patientData << ui->plainTextEditAnamnezNasled->toPlainText();
    patientData << ui->plainTextEditAnamnezRazvitieZabolevaniya->toPlainText();
    patientData << ui->plainTextEditAnamnezVrednPriv->toPlainText();
    patientData << ui->plainTextEditAnamnezBitovieUsl->toPlainText();

    if(!updating)
        dbTable->insertRow(patientData);
    else
        if(dbTable->changeTable(tr("ID"),patientData) == 1)
        {
            updating = false;
            this->on_pushButtonAnamnezSaveImage_clicked();
        }
    updating = true;
    QMessageBox::information(0,"Сохранение","Успех");
}

void AnamnezDial::on_pushButtonAnamnezAdmin_clicked()
{
    this->close();
}

void AnamnezDial::on_pushButtonAnamnezEpicriz_clicked()
{
    this->hide();
    emit(epicButtonSignal());
}


