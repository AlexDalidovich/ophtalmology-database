#include "abstractoperations.h"

#ifndef EPICDBTABLE_H
#define EPICDBTABLE_H

class EpicDbTable : public AbstractOperations
{
    EpicDbTable();

public:
    void createTable();
    AbstractOperations* getPtr();

protected:
    virtual void setPtr(AbstractOperations* _ptr);
private:
    static EpicDbTable* ptr;
    friend class AbstractOperations;
};

#endif // EPICDBTABLE_H
