#include "imagesdbtable.h"

ImagesDbTable::ImagesDbTable()
{
    tableName = "IMAGES";

    unorderedFields<<tr("ID");
    unorderedFields<<tr("RESEARCH");
    unorderedFields<<tr("IMAGE");
    unorderedFields<<tr("WIDTH");
    unorderedFields<<tr("HEIGHT");
    unorderedFields<<tr("DEPTH");
    unorderedFields<<tr("CHANNELS");
    unorderedFields<<tr("ALPHA");

    fields[tr("ID")] = integer;
    fields[tr("RESEARCH")] = text;
    fields[tr("IMAGE")] = image;
    fields[tr("WIDTH")] = integer;
    fields[tr("HEIGHT")] = integer;
    fields[tr("DEPTH")] = integer;
    fields[tr("CHANNELS")] = integer;
    fields[tr("ID_RESEARCH")] = integer;
    fields[tr("ALPHA")] = integer;
}

void ImagesDbTable::createTable()
{
    QString query;
    query = "CREATE TABLE IF NOT EXISTS " + tableName +
            "( ID INTEGER NOT NULL ,"
            "RESEARCH TEXT ,"
            "IMAGE BLOB ,"
            "WIDTH INTEGER ,"
            "HEIGHT INTEGER ,"
            "DEPTH INTEGER ,"
            "CHANNELS INTEGER ,"
            "ALPHA INTEGER ,"
            "ID_RESEARCH INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
            "FOREIGN KEY (ID) REFERENCES VISITS(ID));";
    database.exec(query);
    qDebug()<<"createTable"<<database.lastError();
}


void ImagesDbTable::insertRow(QByteArray *rows)
{
    QSqlQuery query(database);
    query.prepare("INSERT INTO " + tableName +
                  " ( ID ,RESEARCH , IMAGE , WIDTH ,HEIGHT, DEPTH , CHANNELS ,ALPHA  )"
                  " VALUES (:ID,:RESEARCH,:IMAGE,:WIDTH,:HEIGHT,:DEPTH,:CHANNELS,:ALPHA);");
    query.bindValue(":ID",QString(rows[0]));
    query.bindValue(":RESEARCH",QString(rows[1]));
    query.bindValue(":IMAGE",rows[2]);
    query.bindValue(":WIDTH",rows[3]);
    query.bindValue(":HEIGHT",rows[4]);
    query.bindValue(":DEPTH",rows[5]);
    query.bindValue(":CHANNELS",rows[6]);
    query.bindValue(":ALPHA",rows[7]);
    query.exec();
    qDebug()<<query.executedQuery();
    qDebug()<<"image error"<<query.lastError();
}

AbstractOperations *ImagesDbTable::getPtr()
{
    return ptr;
}

void ImagesDbTable::setPtr(AbstractOperations *_ptr)
{
    ptr =(ImagesDbTable*) _ptr;
}

ImagesDbTable* ImagesDbTable::ptr = NULL;
