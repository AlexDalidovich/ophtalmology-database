#ifndef DIAGNOSIS_H
#define DIAGNOSIS_H

#include <QObject>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlError>
#include <QtDebug>

class DiagnosisDbtable : public QObject
{
    Q_OBJECT

    explicit DiagnosisDbtable();

public:
    static DiagnosisDbtable* getPtr();

    void insertRow(QString row);

    QStringList getRows();


private:
    QSqlDatabase database;
    static DiagnosisDbtable* dbTable;
};

#endif
