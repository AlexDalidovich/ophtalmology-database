#include "abstractoperations.h"

#ifndef IMAGESDBTABLE_H
#define IMAGESDBTABLE_H

class ImagesDbTable : public AbstractOperations
{
    ImagesDbTable();

public:
    void createTable();
    void insertRow(QByteArray *rows);
    AbstractOperations* getPtr();

protected:
    virtual void setPtr(AbstractOperations* _ptr);
private:
    static ImagesDbTable* ptr;
    friend class AbstractOperations;
};

#endif // IMAGESDBTABLE_H
