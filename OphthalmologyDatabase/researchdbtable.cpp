#include "researchdbtable.h"

ResearchDbTable::ResearchDbTable()
{
    tableName = "RESEARCH";

    unorderedFields<<tr("ID_RESEARCH");
    unorderedFields<<tr("IMAGE");
    unorderedFields<<tr("ADD_INFO");

    fields[tr("ID_RESEARCH")] = integer;
    fields[tr("IMAGE")] = image;
    fields[tr("ADD_INFO")] = text;
}

void ResearchDbTable::createTable()
{
    QString query;
    query = "CREATE TABLE IF NOT EXISTS "+ tableName +
            " ( ID_RESEARCH NOT NULL ,"
            " IMAGE BLOB ,"
            " ADD_INFO TEXT ,"
            " FOREIGN KEY (ID_RESEARCH) REFERENCES IMAGES(ID_RESEARCH)) ON DELETE CASCADE;";
    database.exec(query);

}

AbstractOperations *ResearchDbTable::getPtr()
{
    return ptr;
}

void ResearchDbTable::setPtr(AbstractOperations *_ptr)
{
    ptr = (ResearchDbTable*) _ptr;
}

ResearchDbTable* ResearchDbTable::ptr = NULL;
