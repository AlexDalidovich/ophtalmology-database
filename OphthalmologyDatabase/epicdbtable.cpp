#include "epicdbtable.h"

EpicDbTable::EpicDbTable()
{
    tableName = "EPICRISIS";

    unorderedFields<<tr("ID");
    unorderedFields<<tr("DIAGNOSIS");
    unorderedFields<<tr("THERAPY");

    fields[tr("ID")] = integer;
    fields[tr("DIAGNOSIS")] = text;
    fields[tr("THERAPY")] = text;
}

void EpicDbTable::createTable()
{
    QString query;
    query = "CREATE TABLE IF NOT EXISTS " + tableName +
            "( ID INTEGER NOT NULL UNIQUE,"
            "DIAGNOSIS TEXT ,"
            "THERAPY TEXT, "
            "FOREIGN KEY (ID) REFERENCES VISITS(ID));";
    database.exec(query);
}

AbstractOperations *EpicDbTable::getPtr()
{
    return ptr;
}

void EpicDbTable::setPtr(AbstractOperations *_ptr)
{
    ptr =(EpicDbTable*) _ptr;
}

EpicDbTable* EpicDbTable::ptr = NULL;
