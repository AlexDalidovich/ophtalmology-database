#include "diagnosisdbtable.h"

DiagnosisDbtable::DiagnosisDbtable()
{
    database = QSqlDatabase::database();
    database.exec("PRAGMA foreign_keys = ON;");

    QString query = "CREATE TABLE IF NOT EXISTS DIAGNOSIS_LIST ( DIAGNOSIS TEXT );";
    QSqlQuery q = database.exec(query);
    qDebug()<<q.lastError();
}

DiagnosisDbtable *DiagnosisDbtable::getPtr()
{
    if(!dbTable)
        dbTable = new DiagnosisDbtable;
    return dbTable;
}

void DiagnosisDbtable::insertRow(QString row)
{
    QString query = "INSERT INTO DIAGNOSIS_LIST (DIAGNOSIS) VALUES ('";
    query += row + "' ) ;";
    qDebug()<<query;
    qDebug()<<database.exec(query).lastError();
}

QStringList DiagnosisDbtable::getRows()
{
    QStringList list;
    QString query = "SELECT * FROM DIAGNOSIS_LIST";
    QSqlQuery result = database.exec(query);

    while(result.next())
        list << result.value(result.record().indexOf("DIAGNOSIS")).toString();
    return list;
}

DiagnosisDbtable* DiagnosisDbtable::dbTable = NULL;
