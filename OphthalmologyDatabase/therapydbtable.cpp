#include "therapydbtable.h"

TherapyDbTable::TherapyDbTable(QObject *parent) :
    QObject(parent)
{
    database = QSqlDatabase::database();
    database.exec("PRAGMA foreign_keys = ON;");

    QString query = "CREATE TABLE IF NOT EXISTS THERAPY_LIST ( THERAPY TEXT );";
    database.exec(query);
}

TherapyDbTable *TherapyDbTable::getPtr()
{
    if(!dbTable)
        dbTable = new TherapyDbTable;
    return dbTable;
}

void TherapyDbTable::insertRow(QString row)
{
    QString query = "INSERT INTO THERAPY_LIST (THERAPY) VALUES ('";
    query += row + "' ) ;";
    database.exec(query);
}

QStringList TherapyDbTable::getRows()
{
    QStringList list;
    QString query = "SELECT * FROM THERAPY_LIST";
    QSqlQuery result = database.exec(query);

    while(result.next())
        list << result.value(result.record().indexOf("THERAPY")).toString();
    return list;
}

TherapyDbTable* TherapyDbTable::dbTable = NULL;
