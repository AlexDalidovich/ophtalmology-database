#include "anamnesisdbtable.h"

AnamnesisDbTable::AnamnesisDbTable()
{
    tableName = "ANAMNESIS";

    unorderedFields<<tr("ID");
    unorderedFields<<tr("COMPLAINS");
    unorderedFields<<tr("PRESSURE");
    unorderedFields<<tr("EYE_L");
    unorderedFields<<tr("EYE_R");
    unorderedFields<<tr("INTOLERANCE");
    unorderedFields<<tr("LAST_DISEASES");
    unorderedFields<<tr("CURRENT_DISEASES");
    unorderedFields<<tr("GENETICS");
    unorderedFields<<tr("DISEASES_PROGRESS");
    unorderedFields<<tr("BAD_HABBITS");
    unorderedFields<<tr("LIFE_CONDITIONS");

    fields[tr("ID")] = integer;
    fields[tr("COMPLAINS")] = text;
    fields[tr("PRESSURE")] = real;
    fields[tr("EYE_L")] = real;
    fields[tr("EYE_R")] = real;
    fields[tr("INTOLERANCE")] = text;
    fields[tr("LAST_DISEASES")] = text;
    fields[tr("CURRENT_DISEASES")] = text;
    fields[tr("GENETICS")] = text;
    fields[tr("DISEASES_PROGRESS")] = text;
    fields[tr("BAD_HABBITS")] = text;
    fields[tr("LIFE_CONDITIONS")] = text;
}

void AnamnesisDbTable::createTable()
{
    QString query;
    query = "CREATE TABLE IF NOT EXISTS " + tableName +
            "( ID INTEGER NOT NULL UNIQUE ,"
            "COMPLAINS TEXT ,"
            "PRESSURE REAL ,"
            "EYE_L REAL ,"
            "EYE_R REAL ,"
            "INTOLERANCE TEXT ,"
            "LAST_DISEASES TEXT ,"
            "CURRENT_DISEASES TEXT ,"
            "GENETICS TEXT ,"
            "DISEASES_PROGRESS TEXT ,"
            "BAD_HABBITS TEXT ,"
            "LIFE_CONDITIONS TEXT , "
            "FOREIGN KEY (ID) REFERENCES VISITS(ID));";
    database.exec(query);
}

AbstractOperations *AnamnesisDbTable::getPtr()
{
    return ptr;
}

void AnamnesisDbTable::setPtr(AbstractOperations *_ptr)
{
    ptr =(AnamnesisDbTable*) _ptr;
}

AnamnesisDbTable* AnamnesisDbTable::ptr = NULL;
