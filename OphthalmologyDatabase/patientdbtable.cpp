#include "patientdbtable.h"

PatientDbTable::PatientDbTable()
{


    tableName = "PATIENT";

    unorderedFields<<tr("AMB_NUM");
    unorderedFields<<tr("BIRTHDAY");
    unorderedFields<<tr("SEX");
    unorderedFields<<tr("ADDRESS");
    unorderedFields<<tr("TEL");
    unorderedFields<<tr("JOB");
    unorderedFields<<tr("POST");
    unorderedFields<<tr("PAS_SERIE");
    unorderedFields<<tr("PAS_NUM");
    unorderedFields<<tr("PAS_GIVEN");
    unorderedFields<<tr("PAS_ADDRESS_GIVEN");
    unorderedFields<<tr("ADD_INFO");
    unorderedFields<<tr("FIRST_NAME");
    unorderedFields<<tr("SECOND_NAME");
    unorderedFields<<tr("LAST_NAME");

    fields[tr("AMB_NUM")] = integer;
    fields[tr("BIRTHDAY")] = text;
    fields[tr("SEX")] = integer;
    fields[tr("ADDRESS")] = text;
    fields[tr("TEL")] = text;
    fields[tr("JOB")] = text;
    fields[tr("POST")] = text;
    fields[tr("PAS_SERIE")] = text;
    fields[tr("PAS_NUM")] = integer;
    fields[tr("PAS_GIVEN")] = text;
    fields[tr("PAS_ADDRESS_GIVEN")] = text;
    fields[tr("ADD_INFO")] = text;
    fields[tr("FIRST_NAME")] = text;
    fields[tr("SECOND_NAME")] = text;
    fields[tr("LAST_NAME")] = text;
}

void PatientDbTable::createTable()
{
    QString query;
    query = "CREATE TABLE IF NOT EXISTS " + tableName +
            "( AMB_NUM INTEGER NOT NULL PRIMARY KEY,"
            "BIRTHDAY TEXT ,"
            "SEX INTEGER ,"
            "ADDRESS TEXT ,"
            "TEL TEXT ,"
            "JOB TEXT ,"
            "POST TEXT ,"
            "PAS_SERIE TEXT ,"
            "PAS_NUM INTEGER ,"
            "PAS_GIVEN TEXT ,"
            "PAS_ADDRESS_GIVEN TEXT ,"
            "ADD_INFO TEXT ,"
            "FIRST_NAME TEXT ,"
            "SECOND_NAME TEXT ,"
            "LAST_NAME TEXT );";
    database.exec(query);
}

AbstractOperations *PatientDbTable::getPtr()
{
    return ptr;
}

int PatientDbTable::changeTable(QString &cell, QStringList &list)
{
    QString query;
    query = "UPDATE " + tableName + " SET ";
    for (int i = 1; i < list.length(); i++)
    {
        query += unorderedFields[i] + " = '" + list[i] + "' ,";
    }
    query.replace(query.length() - 1, 1, " ");
    query += " WHERE " + cell + " = '" +list[0] +"' ;";
    qDebug()<<query;
    qDebug()<<"patient "<<database.exec(query).lastError();
    return database.exec(query).lastError().number();
}

void PatientDbTable::setPtr(AbstractOperations *_ptr)
{
    ptr =(PatientDbTable*) _ptr;
}

PatientDbTable* PatientDbTable::ptr = NULL;
