#include "abstractoperations.h"

QString &AbstractOperations::getStringType(AbstractOperations::DataType type)
{
    return tr(typeString[type]);
}

AbstractOperations::AbstractOperations()
{
    database = QSqlDatabase::database();
    database.exec("PRAGMA foreign_keys = ON;");
}

QString AbstractOperations::getTableName()
{
    return tableName;
}

int AbstractOperations::insertRow(QStringList& rows)
{
        QSqlQuery query(database);

        for(int i = 0; i < rows.length() ; i++)
            rows[i].replace('\'',' ');


        QString queryText = "INSERT INTO " + tableName + " (";
        foreach(QString value, unorderedFields)
            queryText += value + " ,";
        queryText = queryText.replace( queryText.length() - 1, 1, ")" );
        queryText += " VALUES (";
        foreach (QString value, unorderedFields) {
            queryText += "' :" + value + " ',";
        }
        queryText = queryText.replace( queryText.length() - 1, 1, ")" );
        queryText += ";";
        qDebug()<<queryText;

        for(int i = 0; i < unorderedFields.length() ; i++)
            queryText.replace(tr(":") + unorderedFields[i] ,rows[i] );
qDebug()<<"dsfsd"<<rows[fields.size()-2];
        qDebug()<<queryText<<endl;
        if(!query.exec(queryText))
        {
            qDebug()<<"bad require";
            qDebug()<<query.lastError();
        }
        else emit(update());
        return query.lastError().number();
}

void AbstractOperations::changeRow(QString &cell, QString &value )
{
    QString query;
    query = " REPLACE INTO " + tableName +
            " ( " + cell + " ) VALUES ('" + value +
            "') ";
    database.exec(query);
}

int AbstractOperations::changeTable(QString &cell, QStringList &list)
{
    QString query;
    query = "INSERT OR REPLACE INTO " + tableName + " ( ";
    for (int i = 0; i < list.length(); i++)
    {
        query += unorderedFields[i] + " ,";
    }
    query.replace(query.length() - 1, 1, ") ");
    query += " VALUES (";
    for (int i = 0; i < list.length(); i++)
    {
        query += " '" +  list[i] + "' ,";
    }
    query.replace(query.length() - 1, 1, ");");
    QSqlQuery q = database.exec(query);
    q.next();
    qDebug()<<"UPDATE"<<q.record().value(0).toString();
    return q.lastError().number();
}

QSqlQuery AbstractOperations::getFullTable()
{
    QSqlQuery query(database);
    query.exec("SELECT * FROM " + tableName + ";");

    return query;
}

QSqlQuery AbstractOperations::getRows(QString &column, QString &value)
{
    QSqlQuery query(database);
    QString queryText = "SELECT * FROM " + tableName +
            " WHERE ( " + column + " = '" + value + "' );";
    query.exec(queryText);
qDebug()<<query.lastError();
    return query;
}

QSqlQuery AbstractOperations::query(QString text)
{
    QSqlQuery query(database);
    query.exec(text);
    qDebug()<<"query"<<query.lastError();
    return query;
}

QSqlQuery AbstractOperations::deleteRow(QString &column, QString &value)
{
    QSqlQuery query(database);
    QString queryText = "DELETE FROM " + tableName +
            " WHERE ( " + column + " = '" + value + "' );";
    query.exec(queryText);
qDebug()<<query.lastError();
    return query;
}

QHash<QString, QString> AbstractOperations::getTextFields()
{
    return textFields;
}

QHash<QString, int> AbstractOperations::getIntFields()
{
    return integerFields;
}

QHash<QString, float> AbstractOperations::getFloatFields()
{
    return floatFields;
}

QHash<QString, QByteArray> AbstractOperations::getImageFields()
{
    return imageFields;
}

void AbstractOperations::parseRow(QSqlQuery query)
{
    QSqlRecord record = query.record();

        foreach (QString var, fields.keys()) {
            switch (fields[var]) {
            case integer:
                integerFields[var] = query.value(record.indexOf(var)).toInt();
                break;
            case real:
                floatFields[var] = query.value(record.indexOf(var)).toFloat();
                break;
            case text:
                textFields[var] = query.value(record.indexOf(var)).toString();
                break;
            case image:
                imageFields[var] = query.value(record.indexOf(var)).toByteArray();
                break;
            }
        }

}

 const char* AbstractOperations::typeString[] = {
     "INTEGER",
     "REAL",
     "TEXT",
     "BLOB"
 };

