#include "visitdbtable.h"

VisitDbTable::VisitDbTable()
{
    tableName = "VISITS";

    unorderedFields<<tr("AMB_NUM");
    unorderedFields<<tr("VISIT_NUM");
    unorderedFields<<tr("DATE");
    unorderedFields<<tr("DOCTOR");

    fields[tr("ID")] = integer;
    fields[tr("AMB_NUM")] = integer;
    fields[tr("VISIT_NUM")] = integer;
    fields[tr("DATE")] = text;
    fields[tr("DOCTOR")] = text;
}

void VisitDbTable::createTable()
{
    QString query;
    query = "CREATE TABLE IF NOT EXISTS " + tableName +
            " ( ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ,"
            "AMB_NUM INTEGER NOT NULL ,"
            "VISIT_NUM INTEGER ,"
            "DATE TEXT ,"
            "DOCTOR TEXT ,"
            "FOREIGN KEY (AMB_NUM) REFERENCES PATIENT(AMB_NUM));";

    database.exec(query);
}

AbstractOperations *VisitDbTable::getPtr()
{
    return ptr;
}

void VisitDbTable::setPtr(AbstractOperations *_ptr)
{
    ptr =(VisitDbTable*) _ptr;
}


VisitDbTable *VisitDbTable::ptr = NULL;
