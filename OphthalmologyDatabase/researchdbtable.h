#include "abstractoperations.h"

#ifndef RESEARCH_H
#define RESEARCH_H

class ResearchDbTable: public AbstractOperations
{
    ResearchDbTable();
public:

    void createTable();
    AbstractOperations* getPtr();

protected:
    virtual void setPtr(AbstractOperations* _ptr);
private:
    static ResearchDbTable* ptr;
    friend class AbstractOperations;

};





#endif
