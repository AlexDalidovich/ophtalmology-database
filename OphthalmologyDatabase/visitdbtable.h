#include "abstractoperations.h"

#ifndef VISITDBTABLE_H
#define VISITDBTABLE_H

class VisitDbTable : public AbstractOperations
{
    VisitDbTable();
public:
    void createTable();
    AbstractOperations* getPtr();

protected:
    virtual void setPtr(AbstractOperations* _ptr);

private:
    static VisitDbTable* ptr;
    friend class AbstractOperations;
};

#endif // VISITDBTABLE_H
