#include <QObject>
#include <QStringList>
#include <QSqlDatabase>
#include <QHash>
#include <QSqlQuery>
#include <QVariant>
#include <QSqlRecord>
#include <QImage>
#include <QtDebug>

#include <QSqlError>
#include <QHash>

#ifndef ABSTRACTOPERATIONS_H
#define ABSTRACTOPERATIONS_H

class AbstractOperations : public QObject
{
    Q_OBJECT
public:
    enum DataType{
        integer = 0,
        real = 1,
        text = 2,
        image = 3
    } ;
    static QString& getStringType(DataType type);
private:
  const static char *typeString[];

public:
    AbstractOperations();
    QString getTableName();
    virtual void createTable() = 0;
    int insertRow(QStringList &rows);
    void changeRow(QString &cell, QString &value );
    virtual int changeTable(QString& cell, QStringList& list);
    QSqlQuery getFullTable();
    QSqlQuery getRows(QString &column, QString& value);
    QSqlQuery query(QString text);

    QSqlQuery deleteRow(QString &column, QString& value);

    QHash<QString,QString> getTextFields();
    QHash<QString,int> getIntFields();
    QHash<QString,float> getFloatFields();
    QHash<QString,QByteArray> getImageFields();

    void parseRow(QSqlQuery query);

    virtual AbstractOperations* getPtr() = 0;

    template<typename T>
    static T* createDbConnection()
    {
        T* ptr = new T;
        if(!reinterpret_cast<AbstractOperations*>(ptr)->getPtr())
            reinterpret_cast<AbstractOperations*>(ptr)->setPtr(ptr);
        else
        {
            T* temp = ptr;
            ptr = dynamic_cast<T*>(reinterpret_cast<T*>(ptr)->getPtr());
            delete temp;
        }
        return ptr;
    }

signals:
    void update();

protected:
    QHash<QString,DataType> fields;
    QVector<QString> unorderedFields;
    QString tableName;
    QSqlDatabase database;

    virtual void setPtr(AbstractOperations* _ptr) = 0;
private:
    QHash<QString,QString> textFields;
    QHash<QString,int> integerFields;
    QHash<QString,float> floatFields;
    QHash<QString,QByteArray> imageFields;

};

#endif // ABSTRACTOPERATIONS_H
