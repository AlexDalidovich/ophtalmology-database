#include "abstractoperations.h"

#ifndef ANAMNESISDBTABLE_H
#define ANAMNESISDBTABLE_H

class AnamnesisDbTable : public AbstractOperations
{
    AnamnesisDbTable();

public:
    void createTable();
    AbstractOperations* getPtr();

protected:
    virtual void setPtr(AbstractOperations* _ptr);
private:
    static AnamnesisDbTable* ptr;
    friend class AbstractOperations;
};

#endif // ANAMNESISDBTABLE_H
