#include "abstractoperations.h"

#include <QtDebug>

#ifndef PATIENTDBTABLE_H
#define PATIENTDBTABLE_H

class PatientDbTable : public AbstractOperations
{
    PatientDbTable();
public:
    void createTable();
    AbstractOperations* getPtr();

    int changeTable(QString &cell, QStringList &list);
protected:
    virtual void setPtr(AbstractOperations* _ptr);
private:
    static PatientDbTable* ptr;
    friend class AbstractOperations;
};

#endif // PATIENTDBTABLE_H
