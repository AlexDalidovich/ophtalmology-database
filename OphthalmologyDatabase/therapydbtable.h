#ifndef THERAPYDBTABLE_H
#define THERAPYDBTABLE_H

#include <QObject>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QtDebug>

class TherapyDbTable : public QObject
{
    Q_OBJECT
public:
    explicit TherapyDbTable(QObject *parent = 0);

public:
    static TherapyDbTable* getPtr();

    void insertRow(QString row);

    QStringList getRows();

private:
    QSqlDatabase database;
    static TherapyDbTable* dbTable;
};

#endif // THERAPYDBTABLE_H
