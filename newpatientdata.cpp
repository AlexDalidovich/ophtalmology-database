#include "newpatientdata.h"
#include "ui_newpatientdata.h"

newPatientData::newPatientData(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::newPatientData)
{
    ui->setupUi(this);
    this->setAttribute(Qt::WA_DeleteOnClose);

    saved = false;
    updating = false;
    imgSaved = false;

    patientCard = new PatientCard;
    anamnesisDial = new AnamnezDial(this);
    epicDial = new EpicrizDial(this);

    connect(anamnesisDial,SIGNAL(epicButtonSignal()),this,SLOT(on_pushButtonVizitEpicriz_clicked()));
    connect(epicDial,SIGNAL(anamButtonSignal()),this,SLOT(on_pushButtonVizitAnamnez_clicked()));

    connect(anamnesisDial,SIGNAL(destroyed()),this,SLOT(deleteLater()));
    connect(epicDial,SIGNAL(destroyed()),this,SLOT(deleteLater()));

    connect(ui->comboBox,SIGNAL(currentIndexChanged(int)),
            this,SLOT(comboBoxItemSelected(int)));

    ui->pushButtonVizitPatientCard->setHidden(true);
    dialog = NULL;
    img = NULL;
    ui->label_7->setVisible(false);
    ui->pushButton_9->setDisabled(true);
}

newPatientData::~newPatientData()
{
    if(dialog)
        delete dialog;
    if(img)
        cvReleaseImage(&img);
    delete ui;
}

void newPatientData::setAmbNum(int _amb)
{
    amb = _amb;

    VisitDbTable *table = AbstractOperations::createDbConnection<VisitDbTable>();
    QSqlQuery query = table->query("SELECT MAX(VISIT_NUM) FROM " + table->getTableName() +
                     " WHERE ( " + table->getTableName() + ".AMB_NUM = " +
                     QString::number(amb) + " );");
    query.next();
    int visitNum = query.record().value("MAX(VISIT_NUM)").toInt();
    visitNum++;
    ui->label_4->setText(QString::number(visitNum));

    PatientDbTable* patientTable = AbstractOperations::createDbConnection<PatientDbTable>();
    query = patientTable->getRows(tr("AMB_NUM"),QString::number(amb));
    query.next();
    patientTable->parseRow(query);
    QHash<QString,QString> textFields = patientTable->getTextFields();
    ui->lineEdit->setText(textFields[tr("LAST_NAME")] + " " +
            textFields[tr("FIRST_NAME")] + " " +
            textFields[tr("SECOND_NAME")]);
}

void newPatientData::setPatientID(int _id)
{
    id = _id;
    updating = true;

    ui->lineEdit->setReadOnly(true);
    VisitDbTable *table = AbstractOperations::createDbConnection<VisitDbTable>();
    QSqlQuery query = table->getRows(tr("ID"),QString::number(id));
    query.next();
    table->parseRow(query);
    ui->lineEdit_2->setText(table->getTextFields()[tr("DOCTOR")]);
    ui->lineEdit_2->setReadOnly(true);
    ui->label_4->setText(QString::number(table->getIntFields()[tr("VISIT_NUM")]));
    ui->pushButtonVizitPatientCard->setVisible(true);

    this->loadExistingData(id);

}

void newPatientData::on_pushButtonVizitAdmin_clicked()
{
    this->close();
}

void newPatientData::on_pushButtonVizitPatientCard_clicked()
{
    this->hide();
    patientCard->setUpdating(updating);
    patientCard->setAmb(amb);
    patientCard->show();
}

void newPatientData::on_pushButtonVizitAnamnez_clicked()
{
    if(!saved&&!updating)
    {
        QMessageBox::information(0,"Данные не были сохранены","Для перехода на другую вкладку сохраните данные визита");
        return;
    }
    this->hide();
    int tempId;
    if(!updating)
    {
        VisitDbTable* table = AbstractOperations::createDbConnection<VisitDbTable>();

            QSqlQuery query = table->query("SELECT MAX(ID) FROM " + table->getTableName() + ";");
            query.next();
            tempId  = query.record().value("MAX(ID)").toInt();
    }
    else tempId = id;
    anamnesisDial->setUpdatingFlag(updating);
    anamnesisDial->setPatientId(tempId);
    anamnesisDial->show();
}


void newPatientData::on_pushButtonVizitEpicriz_clicked()
{
    if(!saved&&!updating)
    {
        QMessageBox::information(0,"Данные не были сохранены","Для перехода на другую вкладку сохраните данные визита");
        return;
    }
    this->hide();
    int tempId;
    if(!updating)
    {
        VisitDbTable* table = AbstractOperations::createDbConnection<VisitDbTable>();

            QSqlQuery query = table->query("SELECT MAX(ID) FROM " + table->getTableName() + ";");
            query.next();
            tempId  = query.record().value("MAX(ID)").toInt();
    }
    else tempId = id;
    epicDial->setUpdatingFlag(updating);
    epicDial->setPatientName(ui->lineEdit->text());
    epicDial->setPatientId(tempId);
    epicDial->show();
}

void newPatientData::saveData()
{

    if(updating)
        if(!imgSaved)
            updateExisting(id);
        else
            QMessageBox::warning(this,"Нельзя сохранить изображение","Изображение было сохранено \nлибо загружено из базы");
    else saveNew();

    this->loadExistingData(id);

    ui->label_7->setVisible(false);
}

void newPatientData::on_pushButtonVizitSave_clicked()
{
    this->saveData();
}

void newPatientData::saveNew()
{
    if(ui->lineEdit_2->text().isEmpty())
    {
        QMessageBox::warning(this,"Внимание","Введите ФИО врача");
        return;
    }

    VisitDbTable *table = AbstractOperations::createDbConnection<VisitDbTable>();

    QSqlQuery query = table->query("SELECT MAX(VISIT_NUM) FROM " + table->getTableName() +
                 " WHERE ( " + table->getTableName() + ".AMB_NUM = " +
                 QString::number(amb) + " );");
    query.next();
    table->parseRow(query);
    int visitNum = query.record().value("MAX(VISIT_NUM)").toInt();
    QStringList list;
    list<<QString::number(amb);
    list<<QString::number(visitNum + 1);
    list<<QDate::currentDate().toString("dd.MM.yyyy");
    list<<ui->lineEdit_2->text();
    table->insertRow(list);

    query = table->query("SELECT MAX(ID) FROM " + table->getTableName());
    query.next();
    id = query.record().value("MAX(ID)").toInt();

    ui->lineEdit_2->setReadOnly(true);

    updating = true;
    saved = true;

    if(img)
    {
        query = table->query("SELECT MAX(ID) FROM " + table->getTableName());
        query.next();
        id = query.record().value("MAX(ID)").toInt();
        updateExisting(id);
    }

}

void newPatientData::updateExisting(int _id)
{
    ImagesDbTable* imTable = AbstractOperations::createDbConnection<ImagesDbTable>();
    QByteArray rows[8];
    rows[0] = QString::number(_id).toLocal8Bit();
    rows[1] = dialog->getImageType().toLocal8Bit();

    int size = img->height * img->widthStep;
    QDataStream stream(&rows[2],QIODevice::WriteOnly);
    qDebug()<<stream.writeRawData(img->imageData,size);
    rows[3] = QString::number(img->width).toLocal8Bit();
    rows[4] = QString::number(img->height).toLocal8Bit();
    rows[5] = QString::number(img->depth).toLocal8Bit();
    rows[6] = QString::number(img->nChannels).toLocal8Bit();
    rows[7] = QString::number(img->alphaChannel).toLocal8Bit();
    imTable->insertRow(rows);

    QMessageBox::information(this,"Сохранение","Изображение было успешно сохранено");
    imgSaved = true;
}

void newPatientData::loadExistingData(int _id)
{
    ImagesDbTable*  imTable = AbstractOperations::createDbConnection<ImagesDbTable>();
    QSqlQuery query = imTable->getRows(tr("ID"),QString::number(_id));
    ui->comboBox->clear();
    map.clear();
    ui->comboBox->addItem(tr("--"));
    int i = 1;
    while(query.next())
    {
        imTable->parseRow(query);

        int id_research = imTable->getIntFields()[tr("ID_RESEARCH")];
        map[i] = id_research;
        QString resType = imTable->getTextFields()[tr("RESEARCH")];
        ui->comboBox->addItem(resType);
        i++;
    }
}

void newPatientData::loadImage()
{

    ui->pushButton_9->setDisabled(true);
    dialog->close();
    QString filePath = dialog->getFilePath();

    if(img)
    {
        cvReleaseImage(&img);
        img = NULL;
    }

    img = cvLoadImage(filePath.toLocal8Bit());

    QImage* image = new  QImage((uchar*)img->imageData,img->width, img->height,
                        img->widthStep, QImage::Format_RGB888);

    *image = image->rgbSwapped().scaled(ui->label_6->size());
    ui->label_6->setPixmap(QPixmap::fromImage(*image));
    ui->label_7->setVisible(true);
    imgSaved = false;
}

void newPatientData::loadSelectedImage(int id_research)
{
    ImagesDbTable* imTable = AbstractOperations::createDbConnection<ImagesDbTable>();
    QSqlQuery query = imTable->getRows(tr("ID_RESEARCH"),QString::number(id_research));
    query.next();
    imTable->parseRow(query);

    QHash<QString,int> intFields = imTable->getIntFields();
    int w = intFields[tr("WIDTH")];
    int h = intFields[tr("HEIGHT")];
    int depth = intFields[tr("DEPTH")];
    int channels = intFields[tr("CHANNELS")];

    if(img)
        cvReleaseImage(&img);

    img = cvCreateImage(cvSize(w,h),depth,channels);

    memcpy(img->imageData,imTable->getImageFields().begin().value().data(),
           img->widthStep * h);

    QByteArray tempArray = imTable->getImageFields().begin().value();

    this->setImage();

    imgSaved = true;
}

void newPatientData::on_pushButton_clicked()
{
    if(dialog)
        delete dialog;
    dialog = new FileOpenDialog(0,tr("Open file"),tr("C:\\"),tr("Image (*.jpg)"));
    this->setDisabled(true);
    connect(dialog,SIGNAL(accepted()),this,SLOT(loadImage()));
    dialog->setParent(this,Qt::Sheet);
    dialog->show();

}

void newPatientData::comboBoxItemSelected(int index)
{

    if(index > 0)
    {
        this->loadSelectedImage(map[index]);
        ui->pushButton_9->setEnabled(true);
    }
    else ui->pushButton_9->setDisabled(true);
}

void newPatientData::setImage()
{
    if(img)
    {
        QImage image((uchar*)img->imageData,img->width,img->height,QImage::Format_RGB888);
        image = image.rgbSwapped().scaled(ui->groupBox_2->size() - QSize(25,25));
        ui->label_6->resize(image.size() + QSize(3,3));
        ui->label_6->setPixmap(QPixmap::fromImage(image));
    }
}

void newPatientData::resizeEvent(QResizeEvent *event)
{
   this->setImage();
}

void newPatientData::on_pushButton_9_clicked()
{
    QPrinter* printer;
    printer = new QPrinter(QPrinter::PrinterResolution);
    printer->setOutputFormat(QPrinter::NativeFormat);
    printer->setPaperSize(QPrinter::A4);
    printer->setOrientation(QPrinter::Landscape);
    printer->setFullPage(true);
    printer->setResolution(300);
    QImage image((uchar*)img->imageData,img->width,img->height,QImage::Format_RGB888);
    image = image.rgbSwapped();


    QPrintDialog *dlg = new QPrintDialog(printer,0);
    if(dlg->exec() == QDialog::Accepted) {
        QPainter painter(printer);
        QRect rect = painter.viewport();
        QSize size = QSize(ui->lineEdit->width(), ui->lineEdit->height());
        size.scale(rect.size(), Qt::KeepAspectRatio);
        painter.setViewport(10, 60 , ui->lineEdit->width(), ui->lineEdit->height());
        painter.setWindow(QRect(10,60,ui->lineEdit->width(), ui->lineEdit->height()));
        painter.drawText(QPoint(10,60),"Пациент: " + ui->lineEdit->text());

        size = QSize(ui->lineEdit_2->width(), ui->lineEdit_2->height());
        size.scale(rect.size(), Qt::KeepAspectRatio);
        painter.setViewport(10, 160 , ui->lineEdit_2->width(), ui->lineEdit_2->height());
        painter.setWindow(QRect(10,160,ui->lineEdit_2->width(), ui->lineEdit_2->height()));
        painter.drawText(QPoint(10,160),"Врач: " + ui->lineEdit_2->text());

        size = QSize(ui->comboBox->width(), ui->comboBox->height());
        size.scale(rect.size(), Qt::KeepAspectRatio);
        painter.setViewport(10, 260 , ui->comboBox->width(), ui->comboBox->height());
        painter.setWindow(QRect(10,260,ui->comboBox->width(), ui->comboBox->height()));
        painter.drawText(QPoint(10,260),"Исследование: " + ui->comboBox->currentText());

        size = QSize(img->width,img->height);
        size.scale(rect.size(), Qt::KeepAspectRatio);
        painter.setViewport(0, 310 , size.width(), size.height());
        painter.setWindow(QRect(10,40,img->width,img->height));
        painter.drawPixmap(10,40,QPixmap::fromImage(image));
        painter.end();
    }

    delete dlg;
    delete printer;
}
