#ifndef ADDINGDIALOG_H
#define ADDINGDIALOG_H

#include <QDialog>
#include <QtDebug>

namespace Ui {
class AddingDialog;
}

class AddingDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AddingDialog(QWidget *parent = 0);
    ~AddingDialog();

    QString getText();

    void clearInput();

private:
    Ui::AddingDialog *ui;
};

#endif // ADDINGDIALOG_H
