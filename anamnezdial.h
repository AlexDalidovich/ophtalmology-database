#ifndef ANAMNEZDIAL_H
#define ANAMNEZDIAL_H

#include <QDialog>
#include <QCloseEvent>
#include <QSqlField>

#include "newpatientdata.h"
#include "epicrizdial.h"

#include "OphthalmologyDatabase/anamnesisdbtable.h"
#include "OphthalmologyDatabase/visitdbtable.h"

namespace Ui {
class AnamnezDial;
}

class AnamnezDial : public QDialog
{
    Q_OBJECT

public:
    explicit AnamnezDial(QWidget *parent = 0);
    ~AnamnezDial();

    void setPatientId(int &_id);
    void setUpdatingFlag(bool upd);
private slots:
    void on_pushButtonAnamnezSaveImage_clicked();

    void on_pushButtonAnamnezAdmin_clicked();

    void on_pushButtonAnamnezEpicriz_clicked();

public slots:

signals:
    void epicButtonSignal();

private:
    Ui::AnamnezDial *ui;

    AnamnesisDbTable* dbTable;

    int id;
    bool updating;
};

#endif // ANAMNEZDIAL_H
