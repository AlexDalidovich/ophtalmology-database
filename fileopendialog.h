#ifndef FILEOPENDIALOG_H
#define FILEOPENDIALOG_H

#include <QVBoxLayout>
#include <QFileDialog>
#include <QWidget>
#include <QComboBox>

#include <QtDebug>

class FileOpenDialog : public QWidget
{
    Q_OBJECT
public:
    explicit FileOpenDialog(QWidget * parent = 0,
                   const QString & caption = QString(),
                   const QString & directory = QString(),
                   const QString & filter = QString());
    ~FileOpenDialog();

    QString getFilePath();
    QString getImageType();
signals:
    void accepted();
    void rejected();
protected:
    void closeEvent(QCloseEvent *);
private:
    QVBoxLayout* layout;
    QFileDialog* dialog;
    QComboBox* comboBox;
};

#endif // FILEOPENDIALOG_H
