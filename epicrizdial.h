#ifndef EPICRIZDIAL_H
#define EPICRIZDIAL_H

#include <QDialog>
#include <QPushButton>
#include <QVBoxLayout>
#include <QComboBox>

#include "OphthalmologyDatabase/epicdbtable.h"
#include "OphthalmologyDatabase/visitdbtable.h"
#include "OphthalmologyDatabase/patientdbtable.h"
#include "OphthalmologyDatabase/therapydbtable.h"
#include "OphthalmologyDatabase/diagnosisdbtable.h"

#include "addingdialog.h"

namespace Ui {
class EpicrizDial;
}

class EpicrizDial : public QDialog
{
    Q_OBJECT

public:
    explicit EpicrizDial(QWidget *parent = 0);
    ~EpicrizDial();

    void setPatientId(int &_id);
    void setUpdatingFlag(bool upd);
    void setPatientName(QString &name);

signals:
    void anamButtonSignal();

private slots:
    void on_pushButtonEpicrizSave_2_clicked();

    void on_pushButtonEpicrizAdmin_2_clicked();

    void on_pushButtonEpicrizAnamnez_2_clicked();

    void on_pushButtonEpicrizAddDiagnos_2_clicked();
    
    void on_pushButtonEpicrizAddKursLecheniya_2_clicked();

    void addDiagnosis();

    void addTherapy();
    
    void updateBoxes();

    void selectItemFromDiag(int index);

    void selectItemFromTher(int index);

    QStringList getListOfItems(QComboBox* box);

private:
    Ui::EpicrizDial *ui;

    EpicDbTable *dbTable;

    int id;
    bool updating;

    AddingDialog* diagDialog;
    AddingDialog* therDialog;

    QStringList diagList;
    QStringList therList;
};

#endif // EPICRIZDIAL_H
