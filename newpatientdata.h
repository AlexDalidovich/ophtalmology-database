#ifndef NEWPATIENTDATA_H
#define NEWPATIENTDATA_H

#include <QDialog>
#include <QMessageBox>
#include <QDate>
#include <QLabel>

#include "patientcard.h"
#include "anamnezdial.h"
#include "epicrizdial.h"

#include "fileopendialog.h"

#include "OphthalmologyDatabase/visitdbtable.h"
#include "OphthalmologyDatabase/patientdbtable.h"
#include "OphthalmologyDatabase/imagesdbtable.h"

#include <opencv/cv.h>
#include <opencv/cxcore.h>
#include <opencv/highgui.h>

#include <QBuffer>
#include <QPrinter>
#include <QPainter>
#include <QPrintDialog>


class PatientCard;
class AnamnezDial;
class FileOpenDialog;

namespace Ui {
class newPatientData;
}

class newPatientData : public QDialog
{
    Q_OBJECT

public:
    explicit newPatientData(QWidget *parent = 0);
    ~newPatientData();

    void setAmbNum(int _amb);
    void setPatientID(int _id);

private slots:

    void on_pushButtonVizitAdmin_clicked();

    void on_pushButtonVizitPatientCard_clicked();

    void on_pushButtonVizitAnamnez_clicked();

    void on_pushButtonVizitEpicriz_clicked();

    void saveData();

    void on_pushButtonVizitSave_clicked();

    void saveNew();

    void updateExisting(int _id);
    void loadExistingData(int _id);
    void loadImage();
    void loadSelectedImage(int id_research);

    void on_pushButton_clicked();

    void comboBoxItemSelected(int index);

    void setImage();

    void on_pushButton_9_clicked();

protected:
    void resizeEvent ( QResizeEvent * event );

private:
    Ui::newPatientData *ui;

    PatientCard *patientCard;
    AnamnezDial *anamnesisDial;
    EpicrizDial *epicDial;
    FileOpenDialog* dialog;

    IplImage* img;

    int amb;
    int id;
    bool saved;
    bool imgSaved;
    bool updating;

    QMap<int,int> map;
};

#endif // NEWPATIENTDATA_H
