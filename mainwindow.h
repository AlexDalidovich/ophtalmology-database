#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSqlDatabase>
#include <QMessageBox>
#include <QMap>
#include <QList>
#include <QTableWidgetItem>
#include <QPainter>

#include <QtPrintSupport/QPrinter>
#include <QtPrintSupport/QPrintDialog>
#include <QtPrintSupport/QPrintPreviewWidget>

#include "ui_mainwindow.h"
#include "anamnezdial.h"
#include "epicrizdial.h"
#include "newpatientdata.h"
#include "patientcard.h"

#include "OphthalmologyDatabase/patientdbtable.h"
#include "OphthalmologyDatabase/anamnesisdbtable.h"
#include "OphthalmologyDatabase/epicdbtable.h"
#include "OphthalmologyDatabase/visitdbtable.h"
#include "OphthalmologyDatabase/imagesdbtable.h"

#include <QDebug>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();


protected:
    QString& getSelectedCell(QTableWidget* widget, int row, int column);

    void clearTable(QTableWidget* table);


public slots:
    void loadDataOnStart();
    void loadDataFromVisitDb();
    void loadDataFromEpicrisisDb();
    void loadDataFromAnamnesisDb();
    void loadDataFromImageDb();

private slots:

    void loadDataFromPatientDb();

    void on_pushButtonNewPatient_clicked();

    void on_pushButtonOpenPatientData_clicked();

    void on_pushButtonPatientCardOpen_clicked();

    void on_pushButtonEditPatient_clicked();

    void on_tableWidget_cellClicked(int row, int column);

    void on_tableWidget_2_cellClicked(int row, int column);

    void on_tableWidget_3_cellClicked(int row, int column);

    void on_pushButton_5_clicked();

    void on_pushButton_4_clicked();

    void findByTextInName(QString text);

    void findByTextInDiag(QString text);

    void on_pushButton_clicked();

protected:
    void resizeEvent(QResizeEvent* event);

private:
    Ui::MainWindow *ui;

    QSqlDatabase db;
    PatientDbTable* patientDb;
    AnamnesisDbTable* anamnesisDb;
    EpicDbTable* epicrisisDb;
    VisitDbTable* visitDb;
    ImagesDbTable* imagesDb;

    QLabel imLabel;

    QMap<QTableWidgetItem*,int> idMapper;
    QMap<QTableWidgetItem*,int> imMapper;
};

#endif // MAINWINDOW_H
